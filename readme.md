An Exercise for generating wind sway on grass and trees adopted from https://www.youtube.com/watch?v=12gkcdLc77s, https://www.youtube.com/watch?v=eJEpeUH1EMg&feature=youtu.be and https://www.youtube.com/watch?v=L_Bzcw9tqTc
<br/>
Current updates are inspired by https://www.youtube.com/watch?v=d7Vd5oXEJtU&list=PLX2vGYjWbI0RyhAsNJg4sLLKgCZsRSim2&index=5<br/>
<br/>uses
https://sketchfab.com/3d-models/sharp-rock-10-91909721747b46679ee641bb1ffe2ee1<br/>
https://sketchfab.com/3d-models/grass-6d418b9a945e4bee9e9ab7de954d76d1<br/>
https://sketchfab.com/3d-models/old-house-5382f0f9706d47a2acd19ad7222679af<br/>
https://sketchfab.com/3d-models/tree-1e2f0c04b74a4b7ab285d893423fc65a